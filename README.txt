CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Social Post Facebook allows you to configure your site to automatically
publish on a user or organization's Facebook wall. It is based on "Social Post"
and "Social API" projects and is part of the Drupal Social Initiative.

This module defines a path /admin/config/social-api/social-post/facebook,
which displays a configuration form for Social Post Facebook settings.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/social_post_facebook

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/social_post_facebook


REQUIREMENTS
------------

This module requires the following modules:

 * Social API (https://www.drupal.org/project/social_api)
 * Social Post (https://www.drupal.org/project/social_post)


INSTALLATION
------------

 * Download and Install the dependencies: Social API and Social Post
  (Note: Read all the installation guide and README file of these modules
  before proceed).

 * After installed all dependencies install this module as you would normally
  install a contributed Drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-8
  for further information.


CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions:
   - Perform Facebook autoposting

    Users in roles with the "Perform Facebook autoposting" can perform
    Facebook autoposting to the user's accounts.

 * Goto: Administration >> Configuration >> Social API
   (/admin/config/social-api/social-post).
   In this table you can see all the installed 'Social Post' implementers
   (Like: social_post_facebook for Social Post Facebook module).

 * Add your Facebook app OAuth2 information in:
   Administration >> Configuration >> Social API >> Autoposting settings
   (/admin/config/social-api/social-post/facebook)


MAINTAINERS
-----------

Current maintainers:
 * Keegan Rankin (MegaKeegMan) - https://www.drupal.org/u/megakeegman
 * Eleo Basili (Eleonel) - https://www.drupal.org/u/eleonel

This project has been supported by:
 * Agaric
   Agaric helps people create and use powerful web sites.
   Visit: https://agaric.coop for more information.

 * Drutopia
   Drutopia is an initiative to revolutionize the way we build online tools.
   Visit: https://drutopia.org for more information.

<?php

namespace Drupal\social_post_facebook\Controller;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_post\DataHandler;
use Drupal\social_post\Controller\ControllerBase;
use Drupal\social_post\Entity\Controller\SocialPostListBuilder;
use Drupal\social_post\User\UserManager;
use Drupal\social_post_facebook\FacebookPostAuthManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Returns responses for Social Post Facebook routes.
 */
class FacebookPostController extends ControllerBase {

  /**
   * The network plugin manager.
   *
   * @var \Drupal\social_api\Plugin\NetworkManager
   */
  private $networkManager;

  /**
   * The Facebook authentication manager.
   *
   * @var \Drupal\social_post_facebook\FacebookPostAuthManager
   */
  private $facebookManager;

  /**
   * Used to access GET parameters.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $request;

  /**
   * The Social Auth Data Handler.
   *
   * @var \Drupal\social_post\DataHandler
   */
  private $dataHandler;

  /**
   * The data point to be collected.
   *
   * @var string
   */
  private $dataPoints;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The social post user manager.
   *
   * @var \Drupal\social_post\User\UserManager
   */
  protected $userManager;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;


  /**
   * FacebookAuthController constructor.
   *
   * @param \Drupal\social_api\Plugin\NetworkManager $network_manager
   *   Used to get an instance of social_auth_facebook network plugin.
   * @param \Drupal\social_post\User\UserManager $user_manager
   *   Manages user login/registration.
   * @param \Drupal\social_post_facebook\FacebookPostAuthManager $facebook_manager
   *   Used to manage authentication methods.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   Used to access GET parameters.
   * @param \Drupal\social_post\DataHandler $data_handler
   *   The social post data handler.
   * @param \Drupal\social_post\Entity\Controller\SocialPostListBuilder $list_builder
   *   The Social Post entity list builder.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   Used for logging errors.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.

   */
  public function __construct(NetworkManager $network_manager,
                              UserManager $user_manager,
                              FacebookPostAuthManager $facebook_manager,
                              RequestStack $request,
                              DataHandler $data_handler,
                              SocialPostListBuilder $list_builder,
                              LoggerChannelFactoryInterface $logger_factory,
                              MessengerInterface $messenger) {

    $this->networkManager = $network_manager;
    $this->userManager = $user_manager;
    $this->facebookManager = $facebook_manager;
    $this->request = $request;
    $this->dataHandler = $data_handler;
    $this->listBuilder = $list_builder;
    $this->loggerFactory = $logger_factory;
    $this->messenger = $messenger;

    $this->userManager->setPluginId('social_post_facebook');

    // Sets session prefix for data handler.
    $this->dataHandler->getSessionPrefix('social_post_facebook');

    // Sets the plugin id.
    // Sets the session keys to nullify if user could not logged in.
    // $this->facebookManager->setSessionKeysToNullify(['access_token']);.
    $this->setting = $this->config('social_post_facebook.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.network.manager'),
      $container->get('social_post.user_manager'),
      $container->get('facebook_post.social_post_auth_manager'),
      $container->get('request_stack'),
      $container->get('social_post.data_handler'),
      $container->get('entity_type.manager')->getListBuilder('social_post'),
      $container->get('logger.factory'),
      $container->get('messenger')
    );
  }

  /**
   * Redirects the user to FB for authentication.
   */
  public function redirectToFb() {
    /* @var \League\OAuth2\Client\Provider\Facebook false $facebook */
    $facebook = $this->networkManager->createInstance('social_post_facebook')->getSdk();

    // If facebook client could not be obtained.
    if (!$facebook) {
      \Drupal::messenger()->addError($this->t('Social Auth Facebook not configured properly. Contact site administrator.'));
      return $this->redirect('user.login');
    }

    // Facebook service was returned, inject it to $facebookManager.
    $this->facebookManager->setClient($facebook);

    // Generates the URL where the user will be redirected for FB login.
    // If the user did not have email permission granted on previous attempt,
    // we use the re-request URL requesting only the email address.
    $fb_login_url = $this->facebookManager->getAuthorizationUrl();

    $state = $this->facebookManager->getState();

    $this->dataHandler->set('oAuth2State', $state);

    return new TrustedRedirectResponse($fb_login_url);
  }

  /**
   * Response for path 'user/login/facebook/callback'.
   *
   * Facebook returns the user here after user has authenticated in FB.
   */
  public function callback() {
    // Checks if user cancel login via Facebook.
    $error = $this->request->getCurrentRequest()->get('error');
    if ($error == 'access_denied') {
      \Drupal::messenger()->addError($this->t('You could not be authenticated.'));
      return $this->redirect('user.login');
    }

    /* @var \League\OAuth2\Client\Provider\Facebook false $facebook */
    $facebook = $this->networkManager->createInstance('social_post_facebook')->getSdk();
    $this->facebookManager->setClient($facebook);

    // If facebook client could not be obtained.
    if (!$facebook) {
      \Drupal::messenger()->addError($this->t('Social Auth Facebook not configured properly. Contact site administrator.'));
      return $this->redirect('user.login');
    }

    $state = $this->dataHandler->get('oAuth2State');

    // Retrieves $_GET['state'].
    $retrievedState = $this->request->getCurrentRequest()->query->get('state');
    if (empty($retrievedState) || ($retrievedState !== $state)) {
      \Drupal::messenger()->addError($this->t('Facebook login failed. Invalid oAuth2 State.'));
      return $this->redirect('user.login');
    }

    $this->facebookManager->authenticate();

    // User's facebook profile info.
    $userInfo = $this->facebookManager->getUserInfo();
    $profile = $userInfo['user'];
    $pages = $userInfo['pages'];

    if (!$this->userManager->getDrupalUserId($profile->getId())) {
      $this->userManager->addUserRecord($profile->getName(), $this->currentUser()->id(), $profile->getId(), $profile->getLink(), $this->facebookManager->getAccessToken(), serialize($pages));
      $this->messenger->addStatus($this->t('Account added successfully.'));
    }
    else {
      $this->messenger->addWarning($this->t('You have already authorized to post on behalf of this user.'));
    }

    return $this->redirect('entity.user.edit_form', ['user' => $this->currentUser()->id()]);
  }

}

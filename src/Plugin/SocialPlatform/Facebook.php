<?php

namespace Drupal\social_post_controller\Plugin\SocialPlatform;

use Drupal\social_post_controller\SocialPlatformBase;

/**
 * A service for posting Drupal content to Facebook.
 *
 * @SocialPlatform(
 *   id = "facebook",
 *   label = "Facebook",
 *   description = @Translation("Posts to facebook"),
 *   max_char_count = 1000
 * )
 */
class Facebook extends SocialPlatformBase
{

}
